﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using UnityEngine;

public class PlayerLives : MonoBehaviour
{
    public static PlayerLives Singleton { get; private set; } //The singleton for the player's lives
    public static int Lives { get => Singleton.lives; set => Singleton.UpdateLives(Mathf.Clamp(value,0,Singleton.maxlives)); } //The public interface for accessing the lives
    public static int MaxLives => Singleton.maxlives;
    [SerializeField] int maxlives = 3; //The lives to start out at
    [SerializeField] GameObject LivesPrefab; //The prefab for displaying the lives
    private int lives; //The current amount of lives the player has
    // Start is called before the first frame update
    async void Awake()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        await Task.Run(() => Thread.Sleep(100));
        UpdateLives(maxlives);
    }

    private void UpdateLives(int newLives)
    {
        lives = newLives;
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < newLives; i++)
        {
            GameObject.Instantiate(LivesPrefab).transform.SetParent(transform);
        }
    }
}
