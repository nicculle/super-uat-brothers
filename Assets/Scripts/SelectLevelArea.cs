﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelArea : MonoBehaviour
{
    [SerializeField] GameObject LevelButtonPrefab; //The prefab for creating the buttons
    // Start is called before the first frame update
    async void Start()
    {
        //Wait a small amount of time
        await Task.Run(() => Thread.Sleep(100));
        //Loop over all the levels
        for (int i = 0; i < LevelManager.Levels; i++)
        {
            var level = i + 1;
            //Create a new button
            var button = GameObject.Instantiate(LevelButtonPrefab).GetComponent<Button>();
            //Set the text to be the Level number
            button.GetComponentInChildren<Text>().text = $"Level {level}";
            //The button calls LoadLevel if it is pressed
            button.onClick.AddListener(() => LoadLevel(level));
            //Set the button's parent to this
            button.transform.SetParent(transform);
        }
    }

    //Loads the specified level
    private static void LoadLevel(int level)
    {
        //Load the level
        LevelManager.LoadLevel(level);
    }
}
