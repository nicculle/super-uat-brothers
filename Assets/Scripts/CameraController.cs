﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Singleton { get; private set; } //The singleton of the camera controller
    public bool HorizontalLock = false; //If enabled, locks the camera's x-axis to a set value
    public bool VerticalLock = false; //If enabled, locsk the camera's y-axis to a set value
    public float HorizontalLockTarget = 0; //If Horizontal Locking is enabled, this value is used to set the x-axis
    public float VerticalLockTarget = 0; //If Vertical Locking is enabled, this value is used to set the y-axis
    public float MoveSpeed = 10f; //How fast the camera moves towards the target
    public Rect Boundaries = new Rect(0, 0, Mathf.Infinity, Mathf.Infinity); //The area the camera is bound to
    public GameObject Target; //The target the camera will follow

    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    private void Update()
    {
        //If there is a target set
        if (Target != null)
        {
            //Get the target position and make sure it's within the boundaries
            var targetPosition = ClampWithinRect(Target.transform.position);
            //If horizontal locking is enabled
            if (HorizontalLock)
            {
                //Set the x to the HorizontalLockTarget
                targetPosition.Set(HorizontalLockTarget, targetPosition.y, targetPosition.z);
            }
            //If vertical locking is enabled
            if (VerticalLock)
            {
                //Set the y to the VerticalLockTarget
                targetPosition.Set(targetPosition.x, VerticalLockTarget, targetPosition.z);
            }
            //Move towards the target by linear interpolation
            transform.position = Vector3.Lerp(transform.position, new Vector3(targetPosition.x, targetPosition.y, transform.position.z), MoveSpeed * Time.deltaTime);
        }
    }

    //Sets the position of the camera without using interpolation
    public static void SetPosition(Vector2 position)
    {
        var targetPosition = Singleton.ClampWithinRect(new Vector3(position.x,position.y,Singleton.transform.position.z));
        //If horizontal locking is enabled
        if (Singleton.HorizontalLock)
        {
            //Set the x to the HorizontalLockTarget
            targetPosition.Set(Singleton.HorizontalLockTarget, targetPosition.y, targetPosition.z);
        }
        //If vertical locking is enabled
        if (Singleton.VerticalLock)
        {
            //Set the y to the VerticalLockTarget
            targetPosition.Set(targetPosition.x, Singleton.VerticalLockTarget, targetPosition.z);
        }
        Singleton.transform.position = targetPosition;
    }

    //Clamps a position to be within a rect
    private Vector3 ClampWithinRect(Vector3 position)
    {
        return new Vector3(Mathf.Clamp(position.x,Boundaries.xMin,Boundaries.xMax),Mathf.Clamp(position.y,Boundaries.yMin,Boundaries.yMax),position.z);
    }
}
