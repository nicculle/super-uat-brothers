﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpChecker : MonoBehaviour, IOnPlayerTouch
{
    public event Action<Player> OnPlayerJump; //Triggers when the player jumps into the collider
    public void OnPlayerTouch(Player player)
    {
        //If the player is in the air
        if (!player.IsGrounded)
        {
            //Call the OnPlayerJumpEvent
            OnPlayerJump?.Invoke(player);
        }
    }
}
