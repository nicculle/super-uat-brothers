﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Singleton { get; private set; } //The singleton for the Game Manager

    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    //Called when the player hits the winning flag at the end of the level
    public static void WinLevel()
    {
        UIController.SetCurrentState("Win");
    }

    //Called when the player looses all of their lives
    public static void LoseLevel()
    {
        UIController.SetCurrentState("Lose");
    }

    //Called when any "Main Menu" button is pressed
    public void MainMenu()
    {
        UIController.SetCurrentState("Main Menu");
    }

    //Called when the "Next Level" button is pressed
    public void NextLevel()
    {
        LevelManager.LoadLevel(LevelManager.CurrentlyLoadedLevel + 1);
    }

    //Called when the "Retry" button is pressed
    public void RetryLevel()
    {
        LevelManager.LoadLevel(LevelManager.CurrentlyLoadedLevel);
    }

    //Called when the "Quit" button is pressed
    public void QuitGame()
    {
        Application.Quit();
    }

    //Called when the "Play" button is pressed
    public void PlayGame()
    {
        LevelManager.LoadLevel(1);
    }

    //Called when the "Select Level" button is pressed
    public void SelectLevel()
    {
        UIController.SetCurrentState("Level Select");
    }
}
