﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScore : MonoBehaviour
{
    private static int score = 0; //The current score of the game
    public static int Score { get => score; set => UpdateScore(value); } //The public interface for accessing the score
    private static GameScore Singleton; //The singleton for the GameScore
    private TMPro.TextMeshProUGUI text; //The text to display the score

    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //Get the text component
        text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    private static void UpdateScore(int newScore)
    {
        //Update the score
        score = newScore;
        //Update the score text
        Singleton.text.text = newScore.ToString();
    }
}
