﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IOnPlayerTouch
{
    //Called when the player touches the object
    void OnPlayerTouch(Player player);
}
