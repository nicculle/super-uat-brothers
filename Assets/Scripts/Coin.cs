﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SoundContainer;

public class Coin : MonoBehaviour, IOnPlayerTouch
{
    //How many points the coin gives to the player
    public int CoinValue = 10;

    //When the player touches the coin
    public void OnPlayerTouch(Player player)
    {
        //Play the coin collect sound
        AudioSource.PlayClipAtPoint(Sounds.CoinSound, transform.position);
        //Increase the score
        GameScore.Score += CoinValue;
        //Destroy the coin
        Destroy(gameObject);
    }
}
