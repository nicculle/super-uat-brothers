﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SoundContainer;

public class CubeEnemy : MonoBehaviour
{
    protected bool LeftSide => Left.Entered; //If the left side of the sprite has a block there
    protected bool RightSide => Right.Entered; //If the right side of the sprite has a block there
    protected AreaChecker Left; //The area checker on the left side of the enemy
    protected AreaChecker Right; //The area checker on the right side of the enemy
    [SerializeField] protected bool Direction = true; //The direction the enemy is traveling in. If true, the enemy will move to the right
    protected Rigidbody2D rigid; //The rigidbody component of the enemy
    [SerializeField] protected float MovementSpeed = 3f; //How fast the enemy will move per second
    [SerializeField] protected int ScoreReward = 10; //How many points the player will be rewarded if this enemy is killed

    private void Start()
    {
        //Get the area checkers
        var checkers = GetComponentsInChildren<AreaChecker>();
        Left = checkers[0];
        Right = checkers[1];
        //Get the ridid body component
        rigid = GetComponent<Rigidbody2D>();
        //Get the jump checker and add an event for when the player jumps on this enemy
        GetComponentInChildren<JumpChecker>().OnPlayerJump += OnPlayerJump;
    }

    protected virtual void Update()
    {
        //If the direction is to the right
        if (Direction == true)
        {
            //Move the enemy to the right
            rigid.velocity = new Vector2(MovementSpeed, rigid.velocity.y);
            //If there is no ground to the right
            if (RightSide == false)
            {
                //Move to the left
                Direction = false;
            }
        }
        //If the direction is to the left
        else
        {
            //Move the enemy to the left
            rigid.velocity = new Vector2(-MovementSpeed, rigid.velocity.y);
            //If there is no ground to the left
            if (LeftSide == false)
            {
                //Move to the right
                Direction = true;
            }
        }
    }

    //When the player jumps on this enemy
    protected virtual void OnPlayerJump(Player player)
    {
        //Increase the score
        GameScore.Score += ScoreReward;
        //Play the enemy destroy sound
        AudioSource.PlayClipAtPoint(Sounds.EnemyDestroy, transform.position,2);
        //Destroy the enemy
        Destroy(gameObject);
    }
}
