﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public static UIController Singleton { get; private set; } //The singleton for the UI Controlelr
    [SerializeField] string currentState = "Main Menu"; //The current state of the UI
    public static string CurrentState { get => Singleton.currentState; private set => Singleton.currentState = value; } //The public interface for getting the current state

    private static Dictionary<string, GameObject> UIStates = new Dictionary<string, GameObject>(); //The list of possible UI states

    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        //Initialize the list of possible UI states
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            UIStates.Add(child.name, child.gameObject);
        }
        //Update the current state
        SetCurrentState(CurrentState);
    }

    public static void SetCurrentState(string newState)
    {
        //Disable the previous state
        UIStates[CurrentState].SetActive(false);
        //If the new state is valid
        if (UIStates.ContainsKey(newState))
        {
            //Activate the new state
            UIStates[newState].SetActive(true);
            //Set the new state
            CurrentState = newState;
        }
        else
        {
            //Throw an exception
            throw new System.Exception(newState + " is not a valid state");
        }
    }
}
