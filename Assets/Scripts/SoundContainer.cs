﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundContainer : MonoBehaviour
{
    public static SoundContainer Sounds { get; private set; } //The public interface for accessing the sounds
    public AudioClip WalkSoundA; //The walk sound, A variant
    public AudioClip WalkSoundB; //The walk sound, B variant
    public AudioClip JumpSound; //The jump sound
    public AudioClip LandingSound; //The landing sound
    public AudioClip DamageSound; //The damage sound
    public AudioClip CoinSound; //The coin sound
    public AudioClip EnemyDestroy; //The enemy sound

    private void Start()
    {
        //Set the singleton
        if (Sounds == null)
        {
            Sounds = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
