﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinFlag : MonoBehaviour, IOnPlayerTouch
{
    //If the player is touching the flag
    public void OnPlayerTouch(Player player)
    {
        //Disable the player
        player.gameObject.SetActive(false);
        //Win the game
        GameManager.WinLevel();
    }
}
