﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaChecker : MonoBehaviour
{
    public bool Entered { get; private set; } = false; //Returns true if the checker is in a collider
    [SerializeField] LayerMask CollisionLayers; //The layers to collide with

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the collider matches the CollisionLayers Mask
        if (FitsMask(CollisionLayers, collision.gameObject.layer))
        {
            Entered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //If the collider matches the CollisionLayers Mask
        if (FitsMask(CollisionLayers,collision.gameObject.layer))
        {
            Entered = false;
        }
    }

    //Checks if the layers matches the mask
    private static bool FitsMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}
