﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WallType;

public enum WallType
{
    Left,
    Right
}

public class WallCollision : MonoBehaviour
{
    public WallType wallType;
    [HideInInspector]
    public Player player;

    //If the collider is hitting a wall
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (wallType == Left)
        {
            player.HittingLeftWall = true;
        }
        else
        {
            player.HittingRightWall = true;
        }
    }

    //If teh collider is leaving a wall
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (wallType == Left)
        {
            player.HittingLeftWall = false;
        }
        else
        {
            player.HittingRightWall = false;
        }
    }
}
