﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelButton : MonoBehaviour
{
    private void OnEnable()
    {
        //If the currently loaded level is the last level
        if (LevelManager.CurrentlyLoadedLevel == LevelManager.Levels)
        {
            //Disable the button
            gameObject.SetActive(false);
        }
    }
}
