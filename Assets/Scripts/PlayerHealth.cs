﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public static PlayerHealth Singleton { get; private set; } //The singleton for the player's health bar
    private static float healthInternal = 1f;
    public static float Health { get => healthInternal; set => healthInternal = Mathf.Clamp01(value); } //The value of the health bar between 1 and 0

    public float InterpolationSpeed = 7f; //How fast the health bar interpolates to a new value
    private Image image; //The health bar image
    // Start is called before the first frame update
    void Awake()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        image = GetComponent<Image>(); //Get the image component
        image.fillAmount = Health; //Set the base fill amount to the health
    }

    //Sets the health value without interpolation
    public static void SetHealth(float newValue)
    {
        Health = newValue;
        Singleton.image.fillAmount = Health;
    }

    // Update is called once per frame
    void Update()
    {
        //Interpolate to the current health value
        image.fillAmount = Mathf.Lerp(image.fillAmount, Health, InterpolationSpeed * Time.deltaTime);
    }
}
