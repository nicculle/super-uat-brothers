﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] int levels = 1; //The amount of levels in the game
    public static int Levels => Singleton.levels; //The public interface for accessing the levels
    public static int CurrentlyLoadedLevel { get; private set; } = 0; //The currently loaded level

    private static LevelManager Singleton; //The singleton object for accessibility in static methods

    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    //Used to finish the loading process of the level
    static IEnumerator FinishLoading(int levelNumber)
    {
        //Load the level and wait till it finishes
        yield return SceneManager.LoadSceneAsync("Level " + levelNumber, LoadSceneMode.Additive);
        //Update the currently loaded level number
        CurrentlyLoadedLevel = levelNumber;
        //Change the active scene
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level " + levelNumber));
    }

    //Loads a specified level
    public static void LoadLevel(int levelNumber)
    {
        //Change to the game UI
        UIController.SetCurrentState("Game");
        //Unload the previous level
        UnloadCurrentLevel();
        //Finish the loading process
        Singleton.StartCoroutine(FinishLoading(levelNumber));
        //Update the currently loaded level
        CurrentlyLoadedLevel = levelNumber;
    }

    public static void UnloadCurrentLevel()
    {
        //If there is a level already loaded
        if (CurrentlyLoadedLevel != 0)
        {
            //Change the active scene to the Core UI scene
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Core and UI"));
            //Unload the level
            SceneManager.UnloadScene("Level " + CurrentlyLoadedLevel);
            //Set the currently loaded level to zero
            CurrentlyLoadedLevel = 0;
        }
    }
}
