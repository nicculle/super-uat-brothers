﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using static SoundContainer;

public class Player : MonoBehaviour
{
    public static Player Singleton { get; private set; } //The singleton for the player
    public float JumpSpeed = 7f; //How high the player jumps
    public float MovementSpeed = 7f; //How fast the player moves
    public int JumpAmount = 2; //How many times the player can jump in the air
    public bool HittingRightWall = false; //Determines whether the player is touching a wall to the right
    public bool HittingLeftWall = false; //Determines whether the player is touching a wall to the left
    public float HealthReduction = 0.25f; //How much the health is reduced when the player touches an enemy
    public float InvincibilityTime = 1f; //How long the player remains invincible after being hit with an enemy
    public float FlickerTime = 3f; //How long the player flickers after respawning
    public float FlickerRate = 12f; //How many flickers occur per second
    public Vector2 RespawnPoint; //The position the player respawns at
    public bool Dead = false; //Whether the player is dead or not
    [SerializeField] bool Jumping = false; //Whether the player is jumping
    [SerializeField] WallCollision WallLeft; //The player's wall collider to the left
    [SerializeField] WallCollision WallRight; //The player's wall collider to the right
    public bool WalkSoundType = false; //The type of walking sound to play. If true, the A variant plays, and the B variant otherwise
    private bool WalkSoundStorage = false; //The variable to keep track of the previous walk sound type

    private Animator animator; //The animator for the player
    private Rigidbody2D rigid; //The rigidbody component of the player
    private new SpriteRenderer renderer; //The sprite renderer of the player
    private new AudioSource audio; //The Audio Source of the player
    private bool stayingOnCollider = false; //Is true if the player is standing on a collider
    public bool IsGrounded => rigid.velocity.y == 0 && stayingOnCollider; //Whether the player is on the ground or not
    private int jumpCounter = 0; //Keeps track of the amount of times the player jumps
    private float FlickerClock = 0f; //Keeps track of the amount of flickering
    private bool DoFlicker = false; //If true, the player will flicker
    private bool FlickerState = false; //Keeps track of the current flicker state

    void Start()
    {
        //Set the singleton
        Singleton = this;
        //Get the necessary components
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
        //Start the jump handler
        StartCoroutine(JumpHandler());
        //Reset the lives
        PlayerLives.Lives = PlayerLives.MaxLives;
        //Reset the health
        PlayerHealth.SetHealth(1);
        //Set the camera target
        CameraController.Singleton.Target = gameObject;
        //Reset the camera position
        CameraController.SetPosition(transform.position);
        //Update the respawn point
        RespawnPoint = transform.position;
        //Set the player object of the wall colliders
        WallLeft.player = this;
        WallRight.player = this;
    }

    private void OnDestroy()
    {
        //Remove the singleton
        Singleton = null;
    }

    // Update is called once per frame
    void Update()
    {
        //If the walk sound type has changed
        if (WalkSoundType != WalkSoundStorage)
        {
            //Store the current state
            WalkSoundStorage = WalkSoundType;
            //If the state is set to true
            if (WalkSoundStorage == true)
            {
                //Play the A variant
                audio.PlayOneShot(Sounds.WalkSoundA);
            }
            else
            {
                //Play the B variant
                audio.PlayOneShot(Sounds.WalkSoundB);
            }
        }
        //If the player is dead, do not do anything else beyond this point
        if (Dead)
        {
            return;
        }
        //If the Up arrow is pressed and there are still more available jumps
        if (Input.GetKeyDown(KeyCode.UpArrow) && jumpCounter < JumpAmount)
        {
            //Make the player move upwards
            rigid.velocity = new Vector2(rigid.velocity.x, JumpSpeed);
            //Update the jumping variable
            Jumping = true;
            //Play the jump sound
            audio.PlayOneShot(Sounds.JumpSound);
            //Increase the jump counter
            jumpCounter++;
        }
        //If the left arrow is pressed and the player is not hitting a wall to the left
        if (Input.GetKey(KeyCode.LeftArrow) && !HittingLeftWall)
        {
            //Set the "running" animation parameter
            animator.SetBool("Running", true);
            //Flip the sprite to face the left
            renderer.flipX = true;
            //Move the player to the left
            rigid.velocity = new Vector2(-MovementSpeed, rigid.velocity.y);
        }
        //If the right arrow is pressed and the player is not hitting a wall to the right
        if (Input.GetKey(KeyCode.RightArrow) && !HittingRightWall)
        {
            //Set the "running" animation parameter
            animator.SetBool("Running", true);
            //Face the sprite to the right
            renderer.flipX = false;
            //Move the player to the right
            rigid.velocity = new Vector2(MovementSpeed, rigid.velocity.y);
        }
        //If neither the left or right arrows are pressed
        if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            //Set the "running" animation parameter to false
            animator.SetBool("Running", false);
            //Stop the sprite from moving
            rigid.velocity = new Vector2(0, rigid.velocity.y);
        }
        //If the down arrow is pressed
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //Set the crouching parameter to true
            animator.SetBool("Crouching",true);
        }
        //If the down arrow is no longer pressed
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            //Set the crouching parameter to false
            animator.SetBool("Crouching", false);
        }
        //Set the Grounded animation parameter
        animator.SetBool("Grounded", IsGrounded);
        //Set the Jumping animation parameter
        animator.SetBool("Jumping", Jumping);
        //If the player is set to flicker
        if (DoFlicker)
        {
            //Increase the flicker clock
            FlickerClock += Time.deltaTime * FlickerRate;
            //If the Flicker Clock is greater than 1
            if (FlickerClock >= 1)
            {
                //Reset the clock
                FlickerClock = 0;
                //Change the flicker state
                FlickerState = !FlickerState;
                //Update the player visibility
                renderer.enabled = FlickerState;
            }
        }
        //If flickering is set to false
        else
        {
            //Make the player visible
            renderer.enabled = true;
        }

        //If the player is far below the map
        if (transform.position.y <= -25f)
        {
            //Kill the player
            _ = Die();
        }
    }

    //Used to handle jumping
    IEnumerator JumpHandler()
    {
        while (true)
        {
            //Wait untill the player is jumping
            yield return new WaitUntil(() => Jumping);
            //Wait untill the player is moving back down
            yield return new WaitUntil(() => rigid.velocity.y < 0f);
            //Set jumping to false
            Jumping = false;
            //Wait untill the player is jumping again, is on the ground, or if the player dies
            yield return new WaitUntil(() => Jumping || IsGrounded || PlayerHealth.Health == 0);
            //If the player is not jumping, reset the counter
            if (!Jumping)
            {
                jumpCounter = 0;
            }
            //If the player is on the ground, play the landing sound
            if (IsGrounded)
            {
                audio.PlayOneShot(Sounds.LandingSound);
            }
        }
    }

    //Called when the player dies
    private async Task Die()
    {
        //If the player is already dead, stop here
        if (Dead)
        {
            return;
        }
        //Set dead to true
        Dead = true;
        //Play the damage sound
        audio.PlayOneShot(Sounds.DamageSound);
        //Stop the player from moving
        rigid.velocity = Vector2.zero;
        //Set the health to zero
        PlayerHealth.Health = 0;
        //Hide the player
        renderer.enabled = false;
        await Task.Run(() => Thread.Sleep(2000)); //Wait for 2 seconds
        //Reduce the lives
        PlayerLives.Lives--;
        //If there are no lives left
        if (PlayerLives.Lives == 0)
        {
            //Disable the player
            gameObject.SetActive(false);
            //Lose the game
            GameManager.LoseLevel();
            return;
        }
        //Stop the player from moving
        rigid.velocity = Vector2.zero;
        //Move them to the respawn point
        transform.position = RespawnPoint;
        //Set their health back to normal
        PlayerHealth.Health = 1;
        //The player is no longer dead
        Dead = false;
        //Show the player
        renderer.enabled = true;
        //The player is no longer jumping
        Jumping = false;
        //Enable flickering
        DoFlicker = true;
        //Wait a set amount of time
        await Task.Run(() => Thread.Sleep((int)(FlickerTime * 1000f)));
        //Stop the flickering
        DoFlicker = false;
    }

    //Is true if the health is being reduced. Used to keep track invincibility
    private bool ReducingHealth = false;

    private async void ReduceHealth()
    {
        //If the player is dead or invincibility is already taking place, stop here
        if (Dead || ReducingHealth)
        {
            return;
        }
        //Set ReducingHealth to true
        ReducingHealth = true;
        //Decrease the player's health
        PlayerHealth.Health -= HealthReduction;
        //If the player's health is zero
        if (PlayerHealth.Health == 0)
        {
            //Kill the player
            _ = Die();
            //Set ReducingHealth to false
            ReducingHealth = false;
            return;
        }
        //Play the damage sound
        audio.PlayOneShot(Sounds.DamageSound);
        //Enable flickering
        DoFlicker = true;
        //Wait a set amount of time
        await Task.Run(() => Thread.Sleep((int)(InvincibilityTime * 1000f)));
        DoFlicker = false;
        //Set ReducingHealth to false
        ReducingHealth = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the object is touchable
        var touchable = collision.GetComponent<IOnPlayerTouch>();
        if (touchable != null)
        {
            //Touch the object
            touchable.OnPlayerTouch(this);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        //If the object is harmful
        if (collision.gameObject.layer == LayerMask.NameToLayer("Harmful"))
        {
            //Reduce the health
            ReduceHealth();
        }
        //If the object is safe
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Safe"))
        {
            //Set StayingOnCollider to true
            stayingOnCollider = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //If the object is safe
        if (collision.gameObject.layer == LayerMask.NameToLayer("Safe"))
        {
            //Set StayingOnCollider to false
            stayingOnCollider = false;
        }
    }
}
