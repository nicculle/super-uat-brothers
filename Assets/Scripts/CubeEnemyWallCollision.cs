﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class CubeEnemyWallCollision : CubeEnemy
{
    protected override void Update()
    {
        //If the direction is to the right
        if (Direction == true)
        {
            //Move to the right
            rigid.velocity = new Vector2(MovementSpeed, rigid.velocity.y);
            //If there is a block to the right
            if (RightSide == true)
            {
                //Switch to move to the right
                Direction = false;
            }
        }
        //If the direction is to the right
        else
        {
            //Move to the left
            rigid.velocity = new Vector2(-MovementSpeed, rigid.velocity.y);
            //If there is a block to the left
            if (LeftSide == true)
            {
                //Switch to move to the right
                Direction = true;
            }
        }
    }
}
