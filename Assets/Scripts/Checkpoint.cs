﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour, IOnPlayerTouch
{
    public bool Touched { get; private set; } = false; //Whether the checkpoint has been touched or not
    [SerializeField] Sprite OnTouchSprite; //Used when the checkpoint has been touched

    //When the player touches the checkpoint
    public void OnPlayerTouch(Player player)
    {
        //Update the "Touched" variable
        Touched = true;
        //Update the sprite
        GetComponent<SpriteRenderer>().sprite = OnTouchSprite;
        //Update the player's respawn point
        player.RespawnPoint = transform.position;
        
    }
}
